import { createRouter, createWebHashHistory } from 'vue-router'
import Home from './components/Home.vue'
import User from './components/user.vue'

const routes = [
  { path: '/', component: Home },
  { path: '/:id', component: User },
]

export default createRouter({
  history: createWebHashHistory("/"),
  routes,
})
